<?php

/**
 * @file
 * Feeds parser class for FFFFOUND!
 */

/**
 * Class definition for FFFFOUND! Parser.
 *
 * Parses RSS feeds returned from FFFFOUND!.
 */
class FeedsFFFFOUNDParser extends FeedsParser {

  /**
   * Parse the extra mapping sources provided by this parser.
   *
   * @param FeedsFetcherResult $fetcher_result
   * @param FeedsSource $source
   *
   * @see FeedsParser::parse()
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $ffffound_feed = $fetcher_result->getRaw();
    $result = new FeedsParserResult();

    if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
      @$sxml = simplexml_load_string($ffffound_feed, NULL);
    }
    else {
      @$sxml = simplexml_load_string($ffffound_feed, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
    }

    // Got a malformed XML.
    if ($sxml === FALSE || is_null($sxml)) {
      throw new Exception(t('FeedsFFFFoundParser: Malformed XML source.'));
    }

    $result = $this->parseFFFFOUND($sxml, $source, $fetcher_result);

    return $result;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   */
  public function getMappingSources() {
    return parent::getMappingSources() + array(
      'feed_title' => array(
        'name' => t('Feed title'),
        'description' => t('The title of the pulled feed.'),
      ),
      'guid' => array(
        'name' => t('GUID'),
      ),
      'url' => array(
        'name' => t('FFFFOUND! URL'),
        'description' => t('The URL the the FFFFOUND! page of the image.'),
      ),
      'title' => array(
        'name' => t('Image title'),
        'description' => t('Image title.'),
      ),
      'author' => array(
        'name' => t('Author'),
        'description' => t('The user who ffffound the image.'),
      ),
      'description' => array(
        'name' => t('Description'),
      ),
      'original' => array(
        'name' => t('Original'),
        'description' => t('URL of the original size of the image'),
      ),
      'thumbnail' => array(
        'name' => t('Thumbnail'),
        'description' => t('URL of the thumbnail of the image.'),
      ),
      'source_url' => array(
        'name' => t('Source'),
        'description' => t('URL of the source image.'),
      ),
      'source_referer' => array(
        'name' => t('Referrer'),
        'description' => t('URL of the source referer.'),
      ),
      'savedby_count' => array(
        'name' => t('Saved by'),
        'description' => t('Number of users who saved the image.'),
      ),
      'published_datetime' => array(
        'name' => t('Published on (Datetime)'),
      ),
      'published_timestamp' => array(
        'name' => t('Published on (Timestamp)'),
      ),
    );
  }

  /**
   * Parse FFFFOUND! feed
   *
   * @param SimpleXMLElement $sxml
   * @param FeedsFetcherResult $fetcher_result
   * @param FeedsSource $source
   */
  private function parseFFFFOUND(SimpleXMLElement $sxml, FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $fetcher_result->title = $feed_title = (string) $sxml->title;
    $result = new FeedsParserResult();

    // Iterate over entries in feed
    foreach ($sxml->xpath('//item') as $entry) {

      // Get nodes in media: namespace for media information
      $media = $entry->children('http://search.yahoo.com/mrss/');
      $media_content = $media->content->attributes();
      $media_thumbnail = $media->thumbnail->attributes();

      // Get nodes in ffffound: namespace for FFFFOUND! information
      $ffffound = $entry->children('http://ffffound.com/scheme/feed');
      $ffffound_source = $ffffound->source->attributes();
      $ffffound_savedby = $ffffound->savedby->attributes();

      $item = array(
        'feed_title' => $feed_title,
        'guid' => (string) $entry->guid,
        'url' => (string) $entry->link,
        'title' => (string) $entry->title,
        'author' => (string) $entry->author,
        'description' => html_entity_decode((string) $entry->description),
        'original' => (string) $media_content['url'],
        'thumbnail' => (string) $media_thumbnail['url'],
        'source_url' => (string) $ffffound_source['url'],
        'source_referer' => (string) $ffffound_source['referer'],
        'savedby_count' => (string) $ffffound_savedby['count'],
        'published_datetime' => date('Y-m-d H:i:s', strtotime($entry->pubDate)),
        'published_timestamp' => strtotime($entry->pubDate),
      );

      // Populate the FeedsFetcherResult object with the parsed results.
      $result->items[] = $item;
    }

    return $result;
  }

}
