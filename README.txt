Drupal Feeds FFFFOUND! module:
------------------------------------
Maintainer:
  Per Sandström (http://drupal.org/user/224831)
Requires - Drupal 7
License - GPL (see LICENSE.txt)


Overview:
---------
About FFFFOUND! http://ffffound.com/about!

Feeds FFFFOUND! parses feeds and makes it possible to map all the data from
FFFFOUND! to fields in Drupal.

* FFFFOUND! - http://ffffound.com/


Installation:
------------
1. Make sure you have Feeds installed.
2. Download and place the Feeds FFFFOUND! module in your modules folder (this
   will usually be "sites/all/modules/").
3. Go to "Administer" -> "Site building" -> "Modules" and enable the module.


Configuration:
--------------
Go to "Structure" -> "Feed importers" -> "New importer" to set up the parser.


Contributions:
--------------
* The majority of the code was ported from the Feeds YouTube module,
  by Mohammed J. Razem (http://drupal.org/user/255384)
  and Vojtech Kusy (http://drupal.org/user/56154).
